var APP = angular.module('testApp', ['ui.router']);

APP.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/main/interests');

    $stateProvider
        .state('main', {
          url: '/main',
          templateUrl: 'views/layouts/main.html'
        })
        .state('main.interests', {
            url: '/interests',
            templateUrl: 'views/partials/interests.html',
            resolve: {
                interests: function (crud_service) {
                    return crud_service.get_item('interests');
                }
            },
            controller: 'interestsCtrl'
        })
        .state('main.interest', {
            url: '/interest/:id',
            templateUrl: 'views/partials/interest.html',
            controller: 'interestCtrl'
        })
        .state('main.interest.statuses',{
            url: '/statuses/:screen_name',
            templateUrl: 'views/partials/statuses.html',
            controller: 'statusesCtrl'
        })
        .state('main.new', {
            url: '/new',
            templateUrl: 'views/forms/newInterest.html',
            controller: 'interestCtrl'
        })
        .state('main.edit', {
            url: '/edit/:id',
            templateUrl: 'views/forms/editInterest.html',
            controller: 'interestCtrl'
        });

});