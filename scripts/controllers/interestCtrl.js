(function () {

    var interestCtrl = function ($scope, crud_service, $location, $stateParams) {
        $scope.interest = null;
        if ($stateParams.id) {
            crud_service.get_item('interests/'+$stateParams.id).then(function (response) {
                $scope.interest = response.data;
                console.log("Interest from ID", $scope.interest);
                if (response.data.hashtags != null && response.data.hashtags.length > 0) {
                    $scope.interest.hashtags = '#' + response.data.hashtags.join('#');
                }
                if (response.data.user_mentions != null && response.data.user_mentions.length > 0) {
                    $scope.interest.user_mentions = '@' + response.data.user_mentions.join('@');
                }
            }, function (error) {
                console.log("error", error);
            })
        }

        $scope.createInterest = function () {
            console.log('Create interest', $scope.interest);
            var data = {"interest" : $scope.interest};
            crud_service.post_item('interests', data).then(function (response) {
                $location.path('/main/interests')
            }, function (error) {
                console.log('Response error', error);
            });
        };
        
        $scope.updateInterest = function () {
            console.log('Update interes', $scope.interest);
            var data = {"interest" : $scope.interest};
            crud_service.put_item('interests/'+ $stateParams.id, data).then(function (response) {
                console.log("Update response", response);
                $location.path('/main/interests')
            }, function (error) {
                console.log('Response error', error);
            });
        };
        
    };

    APP.controller('interestCtrl', interestCtrl);
}());