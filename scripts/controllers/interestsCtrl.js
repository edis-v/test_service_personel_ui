(function () {


    var interestsCtrl = function ($scope, interests, crud_service, $timeout) {
        $scope.interests = interests.data;
        console.log($scope.interests[0]._id.$oid);

        $scope.deleteInterest = function (id) {
            crud_service.delete_item('interests/'+id).then(function (response) {
                console.log("Successfully deleted");
                var index;
                for (var i=0; i < $scope.interests.length; i++) {
                    if($scope.interests[i]._id.$oid == id) {
                        index = i;
                    }
                }
                $timeout(function () {
                   $scope.interests.splice(index,1);
                });
            }, function (error) {
                console.log("Error while deleting", error);
            })
        }
    };


    APP.controller('interestsCtrl', interestsCtrl);
}());