(function () {

    var statusesCtrl = function ($scope, $stateParams, crud_service) {
        $scope.loading = true;
        crud_service.get_item($stateParams.screen_name+'/pull?id='+$stateParams.id).then(function (response) {
            console.log("response",response.data);
            $scope.loading = false;
            $scope.statuses = response.data;
        }, function (error) {
            $scope.loading = false;
        });
    };

    APP.controller('statusesCtrl', statusesCtrl);
}());